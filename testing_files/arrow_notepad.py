import datetime
import time
from scrapy import Selector

def parse_game_odds(html):

	all_games = []
	sel = Selector(text=html)

	for game_raw in sel.xpath('//table[@class="table-games sort"]//tbody[not(@class)]').extract():

		games_raw_sel = Selector(text=game_raw)

		scraped_at = datetime.datetime.fromtimestamp(
			time.time()).strftime('%Y-%m-%d %H:%M:%S')

		game_raw_sel = Selector(text=game_raw)
		#game_item = GameOddsItem()
		game_item = {}
		game_item['game_date'] = ''
		game_item['game_time'] = game_raw_sel.xpath('//h3/span/text()').extract_first()
		game_item['scraped_at'] = scraped_at
		game_item['game_type'] = ''#game
		game_item['site_url'] = ''#self.allowed_domains[0]
		game_item['site_name'] = ''#self.name
		game_item['second_half'] = ''#1 if second_half else 0
		game_item['team_one'] = {
			'name' : game_raw_sel.xpath('//tr[contains(@class, "home")]//td[contains(@class, "teamInfo")]//div[@class="teamName"]/text()').extract_first(),
			'juice' : game_raw_sel.xpath('//tr[contains(@class, "home")]//td[contains(@class, "spread")]/button/@us').extract_first(),
			'spread' : game_raw_sel.xpath('//tr[contains(@class, "home")]//td[contains(@class, "spread")]/button/@threshold').extract_first(),
			'money_line' : game_raw_sel.xpath('//tr[contains(@class, "home")]//td[contains(@class, "moneyLine")]/button/@us').extract_first()
		}
		game_item['team_two'] = {
			'name' : game_raw_sel.xpath('//tr[contains(@class, "away")]//td[contains(@class, "teamInfo")]//div[@class="teamName"]/text()').extract_first(),
			'juice' : game_raw_sel.xpath('//tr[contains(@class, "away")]//td[contains(@class, "spread")]/button/@us').extract_first(),
			'spread' : game_raw_sel.xpath('//tr[contains(@class, "away")]//td[contains(@class, "spread")]/button/@threshold').extract_first(),
			'money_line' : game_raw_sel.xpath('//tr[contains(@class, "away")]//td[contains(@class, "moneyLine")]/button/@us').extract_first()
		}

		all_games.append(game_item)

	return all_games


with open('topbet_ncaa.html', 'r') as f:
	data = f.read()

print parse_game_odds(data)
