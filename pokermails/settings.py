# -*- coding: utf-8 -*-

# Scrapy settings for pokermails project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'pokermails'

SPIDER_MODULES = ['pokermails.spiders']
NEWSPIDER_MODULE = 'pokermails.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36'
ROTATING_PROXY_LIST = '63.141.241.98:16001' #15 mins
#ROTATING_PROXY_LIST = '163.172.36.181:15001' #3 mins

DIMES_USERNAME = '5D2413267'
DIMES_PASSWORD = 'pI57$5uB'

BETDSI_USERNAME = 'DD50722'
BETDSI_PASSWORD = '1Nb3G$&4'
BETDSI_COOKIES_FILE = 'betdsi_cookies.txt'
BETDSI_LAST_HTML_FILE = 'betdsi_last_file.html'


POSTGRE_USERNAME = 'admin'
POSTGRE_PASSWORD = 'pokermails'
POSTGRE_HOST = '78.47.45.55'
POSTGRE_PORT = '5432'
POSTGRE_DATABASE = 'pokermails'


DBC_USERNAME = 'bongoglass'
DBC_PASSWORD = 'test123'
NITROGEN_CAPTCHA_FILE = 'nitrogen_captcha.jpg'


PYTHON_REQUESTS_PROXY = {
  'http': 'http://{}'.format(ROTATING_PROXY_LIST),
  'https': 'http://{}'.format(ROTATING_PROXY_LIST),
}

# Obey robots.txt rules
ROBOTSTXT_OBEY = False
#HTTPERROR_ALLOWED_CODES = [503]
HTTPERROR_ALLOW_ALL = True

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See http://scrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
DEFAULT_REQUEST_HEADERS = {
   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
   'Accept-Language': 'en',
}

# Enable or disable spider middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
SPIDER_MIDDLEWARES = {
    'scrapy.spidermiddlewares.offsite.OffsiteMiddleware': None,
    #'pokermails.middlewares.SpiderRemoveDuplicateItems' : 800
}

# Enable or disable downloader middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'scrapy.downloadermiddlewares.retry.RetryMiddleware' : None,
    'rotating_proxies.middlewares.RotatingProxyMiddleware': 610,
}

# Enable or disable extensions
# See http://scrapy.readthedocs.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See http://scrapy.readthedocs.org/en/latest/topics/item-pipeline.html

ITEM_PIPELINES = {
    'pokermails.pipelines.database.DatabaseExporter': 306,
    'pokermails.pipelines.date.StandardDate': 305,
    'pokermails.pipelines.game_time.FormatGameTime': 304,
    'pokermails.pipelines.juice.CalculateJuice': 303,
    'pokermails.pipelines.team_name.CleanTeamName': 302,
    'pokermails.pipelines.unwanted_chars.CleanUnicode': 301,
    'pokermails.pipelines.none.RemoveNone' : 300,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See http://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
