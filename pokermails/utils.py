
import re

from cookies import Cookie, Cookies
from cookielib import CookieJar
from urlparse import urlparse

from requests.cookies import create_cookie

from scrapy.loader.processors import TakeFirst

import dukpy

def calculate_juice(spread):

    juice = 0
    if type(spread) not in (int, str, unicode, float):
        return ''
    spread_float = float(spread)
    if spread_float == 1.0:
        return "N/A"
    else:
        if 2 > spread_float:
            juice = float(-100) / float((spread_float - 1))
        else:
            juice = float(100) * float((spread_float - 1))

    juice = str('+'+str(int(juice))) if int(juice) > 0 else int(juice)
    return juice


def solve_cloudfare_challenge(html, domain):


    find_second_addition = re.search('(?<=challenge-form\'\);)(.*?)(?=value)', html, re.DOTALL)
    second_addition = find_second_addition.group(0) if find_second_addition else None

    if second_addition:
        find_variables = re.search('(?<=;)(.*?)(?=(-|=|\*|\+))', second_addition, re.DOTALL)
        variables = find_variables.group(0) if find_variables else None

        if variables:
            variables = variables.split('.')
        else:
            raise Exception('Could not find the variable part')

    else:
        raise Exception('Could not find the second addition part')

    variables_str = variables[0] + '={"' + variables[1] + '":'

    first_addition_re = '(?<=' + variables_str + ')' + '(.*?)(?=};)'
    find_first_addition = re.search(first_addition_re, html, re.DOTALL)

    if find_first_addition:
        first_addition = find_first_addition.group(0) + '}'
        challenge_value = '; var challenge = parseInt(' + '.'.join(variables) + ', 10) + ' + str(len(domain)) + ' ; challenge'
        js_raw = variables_str + first_addition + second_addition[0:len(second_addition)-3] + challenge_value
        return dukpy.evaljs(js_raw)

    else:
        raise Exception('Could not find first addition')

def extract_scrapy_cookies(response, domain):

    cookies = []

    for raw_cookie in response.headers.getlist('Set-Cookie'):
        cookie = Cookie.from_string(raw_cookie)
        cookies.append(create_cookie(cookie.name, cookie.value, domain=domain))

    response_cookies = response.request.headers.get('Cookie', None)
    if response_cookies:
        response_cookies = Cookies.from_request(response_cookies)
        for c in response_cookies:
            cookie = response_cookies[c]
            cookies.append(create_cookie(cookie.name, cookie.value, domain=domain))

    jar = CookieJar()
    for cookie in cookies:
        jar.set_cookie(cookie)


    return jar

def strip_spaces(raw_str):
    if isinstance(raw_str, (unicode, str)):
        clean = raw_str.strip()
        return clean.replace('\n', ' ').replace('\t','')
    else:
        return raw_str

class NoneEmpty(object):
    def __call__(self,values):
        if values is None:
            return ''
        elif isinstance(values, (list, tuple)):
            if len(values) == 1 and values[0] == '':
                return ''
        return TakeFirst()(values)

