# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

import arrow

from pokermails.items import GameOddsItem

class StandardDate(object):

    game_date_format = 'dddd, MMMM D, YYYY'
    game_time_format = 'h:mma'

    standard_utc = '-05:00'
    #Convert game_time and game_date to a standard format.
    def process_item(self, item, spider):
        if isinstance(item, GameOddsItem):

            t = arrow.get(item['game_date'] +' '+item['game_time'], self.game_date_format+' '+self.game_time_format)
            if item['site_name'] in ('betdsi', 'pinnacle'):
                t = t.replace(tzinfo='-08:00')

            elif item['site_name'] in ('betonline', 'bovada', 'topbet'):
                t = t.replace(tzinfo='-05:00')

            elif item['site_name'] in ('nitrogensports', 'thegreek'):
                t = t.replace(tzinfo='+01:00')

            elif item['site_name'] == 'bookmaker':
                t = t.replace(tzinfo='-07:00')
            else:
                raise Exception('should not be here')
            t = t.to(self.standard_utc)

            item['game_date'] = t.format(self.game_date_format)
            item['game_time'] = t.format(self.game_time_format)

        return item

