
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, Sequence, Integer, \
                        String, Text, Boolean, DateTime, create_engine

from sqlalchemy.orm import scoped_session, sessionmaker

from scrapy.utils.project import get_project_settings
from scrapy.signals import engine_stopped

from pokermails.items import GameOddsItem

SETTINGS = get_project_settings()
DATABASE = 'postgresql+psycopg2://{}:{}@{}:{}/{}'.format(
        SETTINGS['POSTGRE_USERNAME'], SETTINGS['POSTGRE_PASSWORD'],
        SETTINGS['POSTGRE_HOST'], SETTINGS['POSTGRE_PORT'] , SETTINGS['POSTGRE_DATABASE'])

Base = declarative_base()
engine = create_engine(DATABASE, convert_unicode=True, echo=False)


class DatabaseException(Exception):
    pass

class Spread(Base):

    __tablename__   = "spread"

    scraped_at      = Column(DateTime, primary_key=True)
    game_id         = Column(Integer, ForeignKey('game.id'), primary_key=True)
    site_id         = Column(Integer, ForeignKey('site.id'), primary_key=True)
    team_id         = Column(Integer, ForeignKey('team.id'), primary_key=True)
    spread          = Column(String(18), nullable=True, primary_key=True)
    juice           = Column(String(18), nullable=True, primary_key=True)
    money_line      = Column(String(18), nullable=True, primary_key=True)

class Team(Base):

    __tablename__   = "team"

    team_id_seq     = Sequence('team_id_seq', start=1, increment=1, minvalue=1, metadata=Base.metadata)
    id              = Column(Integer, team_id_seq, server_default=team_id_seq.next_value(), primary_key=True)
    type            = Column(String(64, convert_unicode=True))
    name            = Column(String(64, convert_unicode=True))

class Game(Base):

    __tablename__   = "game"

    game_id_seq     = Sequence('game_id_seq', start=1, increment=1, minvalue=1, metadata=Base.metadata)
    id              = Column(Integer, game_id_seq, server_default=game_id_seq.next_value(), primary_key=True)
    type            = Column(String(32, convert_unicode=True))
    team_one_id     = Column(Integer, ForeignKey('team.id'))
    team_two_id     = Column(Integer, ForeignKey('team.id'))
    second_half     = Column(Integer)
    game_date       = Column(String(64))
    game_time       = Column(String(8))

class Site(Base):

    __tablename__   = "site"

    site_id_seq     = Sequence('site_id_seq', start=1, increment=1, minvalue=1, metadata=Base.metadata)
    id              = Column(Integer, site_id_seq, server_default=site_id_seq.next_value(), primary_key=True)
    name            = Column(String(32, convert_unicode=True))
    url             = Column(String(128), nullable=True)


Base.metadata.create_all(engine)

DBSession = scoped_session(sessionmaker())
DBSession.configure(bind=engine, autoflush=False, expire_on_commit=False)


class DatabaseExporter(object):

    def process_item(self, item, spider):

        if isinstance(item, GameOddsItem):
            site_id = self.process_site(item)
            teams_id = self.process_teams(item)
            game_id = self.process_game(item, teams_id[0], teams_id[1])

            self.add_spread(item, game_id, site_id, teams_id)

        return item

    @classmethod
    def from_crawler(cls, crawler):

        crawler.signals.connect(cls.close_db_session, signal=engine_stopped)

        return cls()

    def close_db_session(self):
        DBSession.close()

    def add_spread(self, item, game_id, site_id, teams_id):

        for i, team_id in enumerate(teams_id):

            team_key = 'team_one' if i == 0 else 'team_two'
            spread_item = Spread()
            spread_item.spread = item[team_key]['spread']
            spread_item.juice = item[team_key]['juice']
            spread_item.money_line = item[team_key]['money_line']
            spread_item.scraped_at = item['scraped_at']
            spread_item.game_id = game_id
            spread_item.site_id = site_id
            spread_item.team_id = team_id

            DBSession.merge(spread_item)

        try:
            DBSession.commit()
        except Exception as e:
            DBSession.rollback()
            raise DatabaseException(e)

    def process_game(self, item, team_one_id, team_two_id):

        has_game = DBSession.query(Game) \
            .filter_by(type=item['game_type']) \
            .filter_by(second_half=item['second_half'])\
            .filter_by(game_time=item['game_time']) \
            .filter_by(game_date=item['game_date']) \
            .filter_by(team_one_id=team_one_id) \
            .filter_by(team_two_id=team_two_id)


        if not has_game.first():

            game_item = Game()
            game_item.type = item['game_type']
            game_item.second_half = item['second_half']
            game_item.game_time = item['game_time']
            game_item.game_date = item['game_date']
            game_item.team_one_id = team_one_id
            game_item.team_two_id = team_two_id

            DBSession.add(game_item)
            DBSession.flush()

            return game_item.id

        else:

            return has_game.first().id

    def process_site(self, item):

        has_site = DBSession.query(Site) \
            .filter_by(name=item['site_name'])

        if not has_site.first():
            site_item = Site()
            site_item.name = item['site_name']
            site_item.url = item['site_url']

            DBSession.add(site_item)
            DBSession.flush()

            return site_item.id

        else:

            return has_site.first().id

    def process_teams(self, item):

        team_ids = []

        for i, team_key in enumerate(('team_one', 'team_two')):
            has_team = DBSession.query(Team) \
                .filter_by(type=team_key) \
                .filter_by(name=item[team_key]['name'])

            if not has_team.first():
                team_item = Team()
                team_item.type = team_key
                team_item.name = item[team_key]['name']

                DBSession.add(team_item)
                DBSession.flush()

                team_ids.append(team_item.id)

            else:

                team_ids.append(has_team.first().id)

        return team_ids


if __name__ == '__main__':
    p = DatabaseExporter()

    i = {"site_name": "topbet", "team_one": {"juice": "-110", "spread": "+10.5", "money_line": "-110", "name": "Valparaiso"}, "team_two": {"juice": "-110", "spread": "-10.5", "money_line": "-110", "name": "Northwestern"}, "game_time": "8:00pm", "game_type": "ncaa", "site_url": "topbet.eu", "second_half": 0, "game_date": "Thursday, December 14, 2017", "scraped_at": "2017-12-14 13:17:19"}

    print p.process_item(i, '')
