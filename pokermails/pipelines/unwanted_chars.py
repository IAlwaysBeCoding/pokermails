# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

from pokermails.items import GameOddsItem

class CleanUnicode(object):

    #Clean ugly unicode characters
    def process_item(self, item, spider):
        self.remove_chars(item, [u'\xc2',u'\xa0'])
        return item

    def remove_chars(self, item, chars):
        for char in chars:
            for k in item:
                if isinstance(item[k] , dict):
                    for kk in item[k]:
                        if item[k][kk] is not None and not isinstance(item[k][kk], (int, float)) :
                            item[k][kk] = item[k][kk].replace(char, '')
                else:
                    if item[k] is not None and not isinstance(item[k], (int, float)):
                        item[k] = item[k].replace(char, '')



