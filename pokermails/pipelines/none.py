# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

from pokermails.items import GameOddsItem

class RemoveNone(object):

    #Replace none with a default values
    def process_item(self, item, spider):
        self.replace_none_values(item)
        return item

    def replace_none_values(self, item, replace_with=''):

        for k in item:
            if isinstance(item[k] , dict):
                for kk in item[k]:
                    if item[k][kk] is None:
                        item[k][kk] = replace_with
            else:
                if item[k] is None:
                    item[k] = replace_with

