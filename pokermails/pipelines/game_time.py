# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

import arrow

from pokermails.items import GameOddsItem
from pokermails.utils import strip_spaces

class FormatGameTime(object):

    game_date_format = 'dddd, MMMM D, YYYY'
    game_time_format = 'h:mma'
    #Convert game_time and game_date to a standard format.
    #game_time will be converted to ex : 7:30PM
    #game_date will be converted to ex : Thursday, November 9, 2017
    def process_item(self, item, spider):

        if isinstance(item, GameOddsItem):


            if (item['site_name'] == 'bovada') or (item['site_name'] == 'nitrogensports'):
                if (item['site_name'] == 'bovada'):
                    item['game_time'] = item['game_time'] / 1000

                t = arrow.get(item['game_time'])
                item['game_time'] = t.format(self.game_time_format)
                item['game_date'] = t.format(self.game_date_format)

            if item['site_name'] == 'betonline':
                item['game_date'] = strip_spaces(item['game_date'].split('-')[0])
                item['game_time'] = arrow.get(item['game_time'], 'HH:mm A').format(self.game_time_format)
                item['game_date'] = arrow.get(item['game_date'], 'dddd, MMM D, YYYY').format(self.game_date_format)

            if item['site_name'] == 'betdsi':

                raw_date = item['game_date'].replace('Date: ', '')

                item['game_date'] = arrow.get(raw_date, 'MM/DD/YYYY').format(self.game_date_format)

            if item['site_name'] == 'bookmaker':
                self.strip_spaces(item, 'game_time')
                item['game_time'] = arrow.get(item['game_time'], 'h:mm A').format(self.game_time_format)
                item['game_date'] = item['game_date'].split('-')[-1]
                self.strip_spaces(item, 'game_date')
                self.bookmaker_year_date_hack(item)

            if item['site_name'] == 'topbet':
                t = arrow.get(item['game_time'], 'YYYY-M-DD\n      HH:mm')
                item['game_time'] = t.format(self.game_time_format)
                item['game_date'] = t.format(self.game_date_format)

            if item['site_name'] == 'thegreek':

                if item['game_type'] == 'mma':
                    raw_date = item['game_date'].split(' | ')
                    item['game_date'] = arrow.get(raw_date[0], 'MMM DD, YYYY').format(self.game_date_format)
                    item['game_time'] = raw_date[1]

                t = arrow.get(item['game_time'], 'h:mm A')
                item['game_time'] = t.format(self.game_time_format)

            if item['site_name'] == 'pinnacle':
                t = arrow.get(item['game_time'])
                item['game_time'] = t.format(self.game_time_format)
                item['game_date'] = t.format(self.game_date_format)

        return item

    def strip_bad_unicode_chars(self, item, key):
        if key in item:
            for uni in [u'\xa0']:
                item[key] = item[key].replace(uni, '')

    def strip_spaces(self, item, key):

        if key in item:
            item[key] = strip_spaces(item[key])

    def bookmaker_year_date_hack(self, item):
        if 'game_date' in item:
            t = arrow.get(item['game_date'], 'MMM D')
            if t.format('MMM') == 'Dec':
                formatted_date = '{} {} 2017'.format(t.format('MMM'), t.format('D'))
            else:
                formatted_date = '{} {} 2018'.format(t.format('MMM'), t.format('D'))

            item['game_date'] = arrow.get(formatted_date, 'MMM D YYYY').format(self.game_date_format)



