# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

from pokermails.items import GameOddsItem
from pokermails.utils import calculate_juice, strip_spaces


class CalculateJuice(object):

    #Calculate the juice based off the spread, and fill it in.
    def process_item(self, item, spider):

        if isinstance(item, GameOddsItem):

            if item['site_name']  == 'nitrogensports':
                self.parse_juice_moneyline_nitrogensports(item)
                self.convert_to_str(item)


            if item['site_name'] ==  'bookmaker':
                self.remove_dash_for_empty_values(item)
                self.change_half_to_decimal(item)

                if item['game_type'] != 'mma':
                    self.split_spread_juice(item)

            if item['site_name'] == 'pinnacle':
                self.convert_to_str(item)

            if item['site_name'] != 'nitrogensports':
                self.change_half_to_decimal(item)
                self.convert_to_str(item)
                self.round_float_to_int(item)
                self.add_plus_sign_to_positive(item)
                self.remove_white_spaces(item)

        return item

    def remove_white_spaces(self, item):
        for team_key in ('team_one', 'team_two'):
            for k in ('spread', 'juice', 'money_line'):
                if item[team_key][k] is not None:
                    item[team_key][k] = strip_spaces(item[team_key][k])

    def remove_plus_minus_sign(self, item):
        for team_key in ('team_one', 'team_two'):
            for k in ('spread', 'juice'):
                item[team_key][k] = ''

    def convert_to_str(self, item):
        for team_key in ('team_one', 'team_two'):
            for k in ('spread', 'juice', 'money_line'):
                item[team_key][k] = str(item[team_key][k])

    def round_float_to_int(self, item):
        for team_key in ('team_one', 'team_two'):
            for k in ('spread', 'juice', 'money_line'):
                if item[team_key][k][-2:] == u'.0':
                    item[team_key][k] = item[team_key][k][0:len(item[team_key][k])-2]

    def remove_dash_for_empty_values(self, item):

        for team_key in ('team_one', 'team_two'):
            for k in ('spread', 'juice', 'money_line'):
                if item[team_key][k] == u'-':
                    item[team_key][k] = ''

    def add_plus_sign_to_positive(self, item):

        for team_key in ('team_one', 'team_two'):
            for k in ('spread', 'money_line', 'juice'):
                if len(item[team_key][k]) > 0:
                    if item[team_key][k][0] not in ('+', '-'):
                        item[team_key][k] = '+'+item[team_key][k]

    def change_half_to_decimal(self, item):

        for team_key in ('team_one', 'team_two'):
            for calc in ('spread', 'juice', 'money_line'):
                if item[team_key][calc] is not None and u'\xbd' in item[team_key][calc]:
                    item[team_key][calc] = item[team_key][calc].replace(u'\xbd', '.5')

    def split_spread_juice(self, item):

        for team_key in ('team_one', 'team_two'):
            if item[team_key]['spread'] != u'-' and item[team_key]['spread'] != '':
                spread = item[team_key]['spread']

                juice = None
                if spread.find('+') == 0 and spread.find('-') == -1:

                    spread = '+' + item[team_key]['spread'].split('+')[1]
                    juice = '+' + item[team_key]['spread'].split('+')[2]

                elif spread.find('+') == 0 and spread.find('-') != -1:
                    spread = item[team_key]['spread'].split('-')[0]
                    juice = '-' + item[team_key]['spread'].split('-')[1]

                elif spread.find('-') == 0 and spread.find('+') == -1:
                    spread = '-' + item[team_key]['spread'].split('-')[1]
                    juice = '-' + item[team_key]['spread'].split('-')[2]

                elif spread.find('-') == 0 and spread.find('+') != -1:
                    spread = item[team_key]['spread'].split('+')[0]
                    juice = '+' + item[team_key]['spread'].split('+')[1]

                item[team_key]['juice'] = juice
                item[team_key]['spread'] = spread

    def parse_juice_moneyline_nitrogensports(self, item):

        for team_key in ('team_one', 'team_two'):

            if item[team_key]['juice'] != '' and item[team_key]['juice'] != None:
                juice = calculate_juice(item[team_key]['juice'])
                item[team_key]['juice'] = juice
            if item[team_key]['money_line'] != '' and item[team_key]['money_line'] != None:
                item[team_key]['money_line'] = calculate_juice(item[team_key]['money_line'])

    def clean_up_dimes(self, item):


        for team_key in ('team_one', 'team_two'):
            if len(item[team_key]['spread']) == 3:
                item[team_key]['juice'] = item[team_key]['spread'][1]
                item[team_key]['spread'] = strip_spaces(item[team_key]['spread'][0].replace('\\r\\n', ''))
            else:
                item[team_key]['juice'] = ''
                item[team_key]['spread'] = ''

            if item[team_key]['money_line'] is None:
                item[team_key]['money_line'] = ''

        self.change_half_to_decimal(item)


