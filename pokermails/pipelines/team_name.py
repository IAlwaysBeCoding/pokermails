# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

from scrapy.exceptions import DropItem

from pokermails.items import GameOddsItem
from pokermails.utils import strip_spaces

class CleanTeamName(object):

    #Clean team names ugly unicode characters, and remove spaces
    def process_item(self, item, spider):

        if isinstance(item, GameOddsItem):
            for team_key in ('team_one', 'team_two'):
                item[team_key]['name'] = self.strip_spaces(item[team_key]['name'])
                item[team_key]['name'] = self.remove_unwanted_unicode(item[team_key]['name'])

            if item['site_name'] == 'topbet':
                for team_key in ('team_one', 'team_two'):
                    item[team_key]['name'] = self.remove_halfs_quarter_text(item[team_key]['name'])

            for team_key in ('team_one', 'team_two'):
                item[team_key]['name'] = item[team_key]['name'].split(u'\xa0')[-1]


            if self.is_empty_team_names(item):
                raise DropItem()
        return item

    def is_empty_team_names(self, item):


        for team_key in ('team_one', 'team_two'):
            if item[team_key]['name'] == '' or item[team_key]['name'] == u'':
                return True

        return False

    def strip_spaces(self, text):
        return strip_spaces(text)

    def remove_unwanted_unicode(self, text):

        for uni in [u'\xa0']:
            text = text.replace(uni, '')

        return text

    def remove_halfs_quarter_text(self, text):

        for unwanted_text in ('(1H)', '(2H)', '(1Q)', '(2Q)', '(3Q)', '(4Q)'):
            text = text.replace(unwanted_text, '')

        return text
