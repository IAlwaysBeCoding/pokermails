# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

import collections
import datetime
import json
import time
import urllib

from furl import furl
from requests import Session
from requests.utils import dict_from_cookiejar
from requests_toolbelt.utils import dump

from scrapy import Selector
from scrapy.http import Request
from scrapy.loader import ItemLoader
from scrapy.loader.processors import SelectJmes
from scrapy.utils.project import get_project_settings

from pokermails.items import GameOddsItem
from pokermails.spiders.base import BaseSpider
from pokermails.utils import solve_cloudfare_challenge, extract_scrapy_cookies
from pokermails.deathbycaptcha import SocketClient


class NitrogensportsSpider(BaseSpider):
    name = 'nitrogensports'
    allowed_domains = ['nitrogensports.eu']
    start_urls = []
    odds_page = {
        'mma': 'https://nitrogensports.eu/sport/mixed-martial-arts/ufc',
        'ncaa': 'https://nitrogensports.eu/sport/basketball/ncaa'
    }
    games = {

        'mma': [
            {
                'parsing_method' : 'parse_mma_odds',
                'second_half' : False,
                'url' : ''
            }
        ],
        'ncaa': [
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : False,
                'url' : ''
            },

        ],
        'nba': [
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : False,
                'url' : ''
            },

        ]

    }


    confirm_bypassed = False

    def __init__(self, *args, **kwargs):

        super(NitrogensportsSpider, self).__init__(*args, **kwargs)
        settings = get_project_settings()
        self.proxies = settings['PYTHON_REQUESTS_PROXY']
        self.dbc_username = settings['DBC_USERNAME']
        self.dbc_password = settings['DBC_PASSWORD']
        self.captcha_file = settings['NITROGEN_CAPTCHA_FILE']

        self.game = kwargs.get('game', None)

    def start_requests(self):

        headers = {
            'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36'
        }
        yield Request('https://nitrogensports.eu',
                    meta={
                        'game': self.game,
                        'handle_httpstatus_list' : [503],
                    },
                    callback=self.bypass_cloudfare)
    def bypass_cloudfare(self, response):


        jschl_vc = response.xpath('//input[@name="jschl_vc"]/@value').extract_first()
        _pass = response.xpath('//input[@name="pass"]/@value').extract_first()
        jschl_answer = solve_cloudfare_challenge(response.body, self.allowed_domains[0])
        query = collections.OrderedDict({
            'jschl_vc' : jschl_vc,
            'pass' : urllib.quote_plus(_pass),
            'jschl_answer' : jschl_answer
        })

        base_url  = 'https://nitrogensports.eu/cdn-cgi/l/chk_jschl'
        f = furl(base_url)
        url = str(f.add(query).url)

        time.sleep(6)
        response.meta.update({'handle_httpstatus_list' : [503]})
        yield Request(url,
                      meta=response.meta,
                      dont_filter=True,
                      callback=self.parse_game_odds)

    def generate_captcha(self, session):

        headers = {
            'accept' : 'image/webp,image/apng,image/*,*/*;q=0.8',
            'accept-encoding' : 'gzip, deflate, br',
            'accept-language' : 'en-US,en;q=0.9',
        }


        captcha = session.get('https://nitrogensports.eu/php/login/generate_captcha.php', headers=headers, proxies=self.proxies)

        with open(self.captcha_file, 'w+') as f:
            f.write(captcha.content)

    def solve_captcha(self, session, csrf_token):

        dbc = SocketClient(self.dbc_username, self.dbc_password)
        dbc.is_verbose = True

        max_tries = 10
        captcha = None

        for current_try in (0, max_tries):
            captcha_solved = self.upload_captcha()

            if captcha_solved:
                accepted_captcha, csrf_token, login_link  = self.submit_captcha(session, captcha_solved, csrf_token)

                if accepted_captcha:
                    return True, csrf_token , login_link

        return False, None, None

    def submit_captcha(self, session, captcha, csrf_token):

        base_url = 'https://nitrogensports.eu/php/login/load_login.php'
        headers = {
            'X-csrftoken' : csrf_token,
            'X-requested-with' : 'XMLHttpRequest',
            'Accept' : 'application/json, text/javascript, */*; q=0.01',
        }
        body = {
            'action' : 'new',
            'HTTP_REFERER' : 'https://nitrogensports.eu/',
            'captcha_code' : captcha
        }


        load_login = session.post(base_url, data=body, headers=headers, proxies=self.proxies)

        resp = load_login.json()

        if resp['errno'] != 0:
            return (False, resp['csrf_token'], None)
        else:
            return (True, resp['csrf_token'], resp['login_link'])

    def upload_captcha(self):

        dbc = SocketClient(self.dbc_username, self.dbc_password)
        dbc.is_verbose = True

        try:
            solved_captcha = dbc.decode(self.captcha_file, 60)

        except Exception:
            raise RuntimeError('Failed to upload captcha file')
            solved_captcha = None

        if solved_captcha:
            return solved_captcha['text']

    def load_login_page(self, response, session):

        base_url = 'https://nitrogensports.eu/php/login/load_login.php'
        body = {
            'action' : 'new',
            'HTTP_REFERER' : 'https://nitrogensports.eu/'
        }

        csrf_token = response.xpath('//meta[@name="csrf_token"]/@content').extract_first()
        headers = {
            'X-csrftoken' : csrf_token,
            'X-requested-with' : 'XMLHttpRequest',
            'Accept' : 'application/json, text/javascript, */*; q=0.01',
        }

        csrf_token = response.xpath('//meta[@name="csrf_token"]/@content').extract_first()
        load_login = session.post(base_url, headers=headers, proxies=self.proxies)
        json_data = load_login.content
        with open('load_login_first.html', 'w+') as f:
            f.write(load_login.content)

        csrf_token = SelectJmes('csrf_token')(json_data)

        headers['X-csrftoken'] = csrf_token

        self.generate_captcha(session)
        solved_captcha, csrf_token, login_link  = self.solve_captcha(session, csrf_token)

        return solved_captcha, csrf_token, login_link

    def find_games(self, game, session, csrf_token, login_link):

        if game == 'ncaa':
            sport = 'Basketball'
            league = 'NCAA'
        elif game == 'nba':
            sport = 'Basketball'
            league = 'NBA'
        elif game == 'mma':
            sport = 'Mixed Martial Arts'
            league = 'UFC'

        body_data = {
            'sport' : sport,
            'league' : league,
            'period_description' : ''
        }
        query_games = session.post(
            'https://nitrogensports.eu/php/query/findgames.php',
            headers={
                'X-csrftoken' : csrf_token,
                'X-requested-with' : 'XMLHttpRequest',
            },
            data=body_data,
            proxies=self.proxies)


        game_odds_items = self.parse_game_odds_items(json_data=query_games.json(), game=game)

        return game_odds_items

    def parse_game_odds_items(self, json_data, game):

        game_times = SelectJmes('data[].startDateTime')(json_data)

        teams = SelectJmes(
            "data[].{ " \
                "is_second_half : period[].period_description, " \
                "team_one : { " \
                    "name : homeTeam_name, " \
                    "spread : period[].spread[2].homeSpread | [0] ," \
                    "money_line : period[].moneyLine[].homePrice | [0], " \
                    "juice : period[].spread[2].awayPrice | [0] " \
                "}, " \
                "team_two: { " \
                    "name : awayTeam_name, " \
                    "spread : period[].spread[2].awaySpread | [0], " \
                    "money_line : period[].moneyLine[].awayPrice | [0], " \
                    "juice : period[].spread[2].awayPrice | [0]" \
                "}" \
            "}")(json_data)

        scraped_at = datetime.datetime.fromtimestamp(
            time.time()).strftime('%Y-%m-%d %H:%M:%S')

        games = []
        for i, game_time in enumerate(game_times):
            game_item = GameOddsItem()
            game_item['scraped_at'] = scraped_at
            game_item['game_date'] = ''
            game_item['game_time'] = game_time
            game_item['game_type'] = game
            game_item['site_url'] = self.allowed_domains[0]
            game_item['site_name'] = self.name

            if teams[i]['is_second_half'] == '2nd Half':
                game_item['second_half'] = 1
            else:
                game_item['second_half'] = 0

            teams[i].pop('is_second_half')

            game_item.update(teams[i])

            games.append(game_item)

        return games

    def parse_game_odds(self, response):

        with open('bypased_nitrogen.html','w+') as f:
            f.write(response.body)
        jar = extract_scrapy_cookies(response, self.allowed_domains[0])
        session = Session()
        session.cookies = jar
        session.headers.update({
            'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36'
        })
        is_loggined, csrf_token, login_link = self.load_login_page(response=response, session=session)


        all_games = []

        if is_loggined:

            if self.game is None:

                for game in self.games:
                    find_games = self.find_games(game=game,
                                                 session=session,
                                                 csrf_token=csrf_token,
                                                 login_link=login_link)
                    if find_games:
                        all_games.extend(find_games)
                    time.sleep(5)
            else:
                find_games = self.find_games(game=self.game,
                                             session=session,
                                             csrf_token=csrf_token,
                                             login_link=login_link)
                if find_games:
                    all_games.extend(find_games)

        if all_games:
            for game in all_games:
                yield game

    def parse_mma_odds(self, response):
        for item_or_request in self.parse_game_odds(response):
            yield item_or_request

    def parse_ncaa_odds(self, response):
        for item_or_request in self.parse_game_odds(response):
            yield item_or_request
