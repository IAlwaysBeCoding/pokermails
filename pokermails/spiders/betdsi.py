# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

import datetime
import json
import re
import time
import urllib
from decimal import Decimal

from requests import Session
from requests.cookies import cookiejar_from_dict, create_cookie, RequestsCookieJar
from requests.utils import dict_from_cookiejar

from furl import furl

from scrapy import Selector
from scrapy.http import Request
from scrapy.loader.processors import SelectJmes
from scrapy.utils.project import get_project_settings

from pokermails.items import GameOddsItem
from pokermails.spiders.base import BaseSpider

class Betdsi(object):

    default_user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36'
    default_cookies_file = 'betsi_cookies.txt'
    default_last_html_file = 'betdsi_last_page.html'

    site_url = 'https://www.betdsi.eu'
    site_name = 'betdsi'

    sports = {
        'ncaa': [
            {
                'league' : 'NCAA',
                'sportType' : 'BASKETBALL',
                'scheduleText' : '-',
                'period' : '0',
                'sizeBody' : 'lg',
                'priceType' : 'A',
                'value' : 'basketball_ncaa',
            },
            {
                'league' : 'NCAA',
                'sportType' : 'BASKETBALL',
                'scheduleText' : '-',
                'period' : '2',
                'sizeBody' : 'lg',
                'priceType' : 'A',
                'value' : 'basketball_ncaa',
            }
        ],
        'nba': [
            {
                'league' : 'NBA',
                'sportType' : 'BASKETBALL',
                'scheduleText' : '-',
                'period' : '0',
                'sizeBody' : 'lg',
                'priceType' : 'A',
                'value' : 'basketball_nba',
            },
            {
                'league' : 'NBA',
                'sportType' : 'BASKETBALL',
                'scheduleText' : '-',
                'period' : '2',
                'sizeBody' : 'lg',
                'priceType' : 'A',
                'value' : 'basketball_nba',
            }
        ],
        'mma': [
            {
                'scheduleText' : '',
                'period' : '0',
                'sizeBody' : 'lg',
                'priceType' : 'A',
            }
        ]
    }

    def __init__(self, user, password, **kwargs):

        self.user = user
        self.password = password
        self.proxies = kwargs.get('proxies', None)
        self.user_agent = kwargs.get('user_agent', self.default_user_agent)
        self.cookies_file = kwargs.get('cookies_file', self.default_cookies_file)
        self.last_html_file = kwargs.get('last_html_file', self.default_last_html_file)

        self.session = self._create_session()
        self._last_html = None

    def _create_session(self):

        s = Session()
        s.headers.update({
            'Accept' : 'application/json, text/javascript, */*; q=0.01',
            'Accept-Encoding' : 'gzip, deflate, br',
            'Accept-Language' : 'en-US,en;q=0.9',
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
            'User-Agent' : self.default_user_agent if not self.user_agent else self.user_agent
        })
        return s

    def _get_json_sports(self, html):
        raw_json_sports = re.search('(?<= var availableCategories = {)(.*?)(?=};)', html, re.DOTALL)

        if raw_json_sports:
            return "{" + raw_json_sports.group(0) + "}"

    def scrape_sport(self, sport):

        headers = {
            'Referer' : 'https://backend.betdsi.eu/eng/pages/showloading/eng'
        }
        if not self._last_html:
            self.load_last_html()

        if not self.is_logged_in():
            raise RuntimeError('Not loggined so cannot scrape')

        raw_json = self._get_json_sports(self._last_html)

        if not raw_json:
            raise ValueError('Cannot find raw json sports')

        if sport not in self.sports:
            raise ValueError('Cannot find sport:{} '.format(sport))

        headers = {
            'Accept' : '*/*',
            'Accept-Encoding' : 'gzip, deflate, br',
            'X-Requested-With' : 'XMLHttpRequest',
            'Referer' : 'https://backend.betdsi.eu/eng/Sportbook',
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        }

        games = []
        if sport == 'mma':

            jmes_query = 'BOXING.keys(@)'
            games_keys = SelectJmes(jmes_query)(json.loads(raw_json))
            if games_keys:

                for gk in games_keys:
                    if 'boxing' in gk:
                        jmes_query = 'BOXING.'+gk+'.{league : subCategory, sportType : SportType, value : subCategoryID, games : scheduleArr.values(@)}'
                        games_schedule = SelectJmes(jmes_query)(json.loads(raw_json))
                        if games_schedule:
                            for g in games_schedule['games']:

                                body = []
                                q = urllib.quote_plus
                                for key in self.sports['mma'][0]:
                                    body.append((key, q(self.sports['mma'][0][key])))


                                body.append(('schedules' + q('[]'), q(g)))
                                body.append(('league', q(games_schedule['league'])))
                                body.append(('value', q(games_schedule['value'])))
                                body.append(('sportType', q(games_schedule['sportType'])))

                                body = '&'.join(['{}={}'.format(k[0], k[1]) for k in body])


                                get_games_data = self.session.post(
                                    'https://backend.betdsi.eu/Sportbook/getLoadLine',
                                    headers=headers,
                                    data=body,
                                    proxies=self.proxies
                                )


                                parsed_games = self.parse_game_odds(get_games_data.content, sport)
                                games.extend(parsed_games)

        else:

            for i, game_period in enumerate(self.sports[sport]):
                body = game_period
                body['schedules'] = ''
                get_games_data = self.session.post(
                    'https://backend.betdsi.eu/Sportbook/getLoadLine',
                    headers=headers,
                    data=body,
                    proxies=self.proxies
                )
                parsed_games = self.parse_game_odds(get_games_data.content, sport)

                games.extend(parsed_games)

        return games

    def parse_game_odds(self, html, game):

        all_games = []
        sel = Selector(text=html)
        last_game_date = None
        for game_raw in sel.xpath('//table[@class="table-games sort"]//tbody[not(@class)]').extract():

            games_raw_sel = Selector(text=game_raw)

            scraped_at = datetime.datetime.fromtimestamp(
                time.time()).strftime('%Y-%m-%d %H:%M:%S')

            game_raw_sel = Selector(text=game_raw)
            game_item = {}
            game_item['game_date'] = game_raw_sel.xpath('//td[@class="headersOverview"]/span/text()').extract_first()
            game_item['game_time'] = game_raw_sel.xpath('//span[@class="date-span"]/text()').extract_first()
            game_item['scraped_at'] = scraped_at
            game_item['game_type'] = game
            game_item['site_url'] = self.site_url
            game_item['site_name'] = self.site_name
            game_item['second_half'] = 1 if game_raw_sel.xpath('//button[@periodNumber="2"]') else 0
            game_item['team_one'] = {
                'name' : game_raw_sel.xpath('//tr[contains(@class, "home")]//td[contains(@class, "teamInfo")]//div[@class="teamName"]/text()').extract_first(),
                'juice' : game_raw_sel.xpath('//tr[contains(@class, "home")]//td[contains(@class, "spread")]/button/@us').extract_first(),
                'spread' : game_raw_sel.xpath('//tr[contains(@class, "home")]//td[contains(@class, "spread")]/button/@threshold').extract_first(),
                'money_line' : game_raw_sel.xpath('//tr[contains(@class, "home")]//td[contains(@class, "moneyLine")]/button/@us').extract_first()
            }
            game_item['team_two'] = {
                'name' : game_raw_sel.xpath('//tr[contains(@class, "away")]//td[contains(@class, "teamInfo")]//div[@class="teamName"]/text()').extract_first(),
                'juice' : game_raw_sel.xpath('//tr[contains(@class, "away")]//td[contains(@class, "spread")]/button/@us').extract_first(),
                'spread' : game_raw_sel.xpath('//tr[contains(@class, "away")]//td[contains(@class, "spread")]/button/@threshold').extract_first(),
                'money_line' : game_raw_sel.xpath('//tr[contains(@class, "away")]//td[contains(@class, "moneyLine")]/button/@us').extract_first()
            }

            if game_item['game_time']:
                game_item['game_time'] = game_item['game_time'].replace(' a', 'am')
                game_item['game_time'] = game_item['game_time'].replace(' p', 'pm')

            if not game_item['game_date']:
                game_item['game_date'] = last_game_date
            else:
                last_game_date = game_item['game_date']

            all_games.append(game_item)

        return all_games

    def login(self):

        def do_login_ajax(session, proxies, user, password):

            body = {
                'user ' : user,
                'username ' : user,
                'password ' : password,
                'ioBB ' : ''
            }

            s = session.post('https://backend.betdsi.eu/pages/dologinajax', data=body, proxies=proxies)
            #with open('do_login_ajax.html', 'w+') as f:
            #    f.write(s.content)

        def wp_content_login(session, proxies, user, password):

            url = 'https://www.betdsi.eu/wp-content/themes/betdsi-eu/dologin.php?user={}&passwd={}'.format(user, password)
            f = furl('https://www.betdsi.eu/wp-content/themes/betdsi-eu/dologin.php')
            f.args['user'] = user
            f.args['passwd'] = password

            s = session.get(str(f.url), headers={'X-Requested-With' : 'XMLHttpRequest'})
            #with open('wp_login_content.html', 'w+') as f:
            #    f.write(s.content)
            try:
                return s.json()['token']
            except :
                return None

        def login_with_token(session, token, proxies, user):

            f = furl('https://backend.betdsi.eu/pages/dologinexternal')

            f.args['token'] = token
            f.args['user'] = user
            f.args['h'] = 'https://www.betdsi.eu'
            f.args['t'] = 'betdsi.eu'
            f.args['l'] = 'eng'
            f.args['rtc'] = ''

            s = session.get(str(f.url), proxies=proxies)
            #with open('do_login_external.html', 'w+') as f:
            #    f.write(s.content)

        def finalize_login(session, proxies):

            def get_betdsi_secret_cookie(html):

                find_numbers = re.search('(?<=var v = )(.*?) \* (.*?)(?=;)', html, re.DOTALL)

                if find_numbers:

                    secret_value = int(Decimal(find_numbers.group(1)) * Decimal(find_numbers.group(2)))

                    find_name = re.search('(?<=document\.cookie = ")(.*?)(?==)', html, re.DOTALL)

                    if find_name:
                        return create_cookie(find_name.group(1), str(secret_value))

            s = session.get('https://backend.betdsi.eu/eng/pages/showloading/eng', proxies=proxies)
            #c = get_betdsi_secret_cookie(s.content)
            #session.cookies.set_cookie(c)
            #with open('beforelastbetdsi.html', 'w+') as f:
            #    f.write(s.content)


            s = session.get('https://backend.betdsi.eu/eng/Sportbook', proxies=proxies)

            #with open('xlastbetdsi.html', 'w+') as f:
            #    f.write(s.content)

            self._last_html = s.content
            self.save_last_html()
            self.save_cookies()


        self.session.get('https://www.betdsi.eu/', proxies=self.proxies)

        do_login_ajax(self.session, self.proxies, self.user, self.password)
        get_token = wp_content_login(self.session, self.proxies, self.user, self.password)

        if not get_token:
            raise RuntimeError('Failed at getting login token')

        login_with_token(self.session, get_token, self.proxies, self.user)
        finalize_login(self.session, self.proxies)

    def load_last_html(self):

        try:
            with open(self.last_html_file, 'r') as f:
                self._last_html = f.read()

            return self._last_html
        except IOError:
            self.login()
            return self._last_html

    def save_last_html(self):
        with open(self.last_html_file, 'w+') as f:
            f.write(self._last_html)

    def save_cookies(self):

        cookies = dict_from_cookiejar(self.session.cookies)
        with open(self.cookies_file, 'w+') as f:
            f.write(json.dumps(cookies))

    def load_cookies(self):

        cookies = json.load(open(self.cookies_file, 'r'))

        self.session.cookies = cookiejar_from_dict(cookies, overwrite=True)

    def is_logged_in(self):

        s = self.session.post('https://backend.betdsi.eu/App/isLogged', headers={'X-Requested-With' : 'XMLHttpRequest'})
        try:
            resp = s.json()

            if resp['status'] == 1:
                return True
            else:
                return False
        except:
            return False
    def get_user_info(self):

        s = self.session.post('https://backend.betdsi.eu/Pages/getInfoCustomer', headers={'X-Requested-With' : 'XMLHttpRequest'})
        return s.content


class BetdsiSpider(BaseSpider):
    name = 'betdsi'
    allowed_domains = ['betdsi.eu']
    start_urls = ['https://www.betdsi.eu']

    games = {
        'mma': [
            {
                'parsing_method':'parse_mma_odds',
                'second_half' : False,
                'url' : 'https://www.betdsi.eu/sportsbook-betting/fighting-odds/',
            },
        ],
        'ncaa': [
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : False,
                'url' : 'https://www.betdsi.eu/sportsbook-betting/ncaab-odds/',

            },
        ],
        'nba': [
            {
                'parsing_method' : 'parse_nba_odds',
                'second_half' : False,
                'url' : 'https://www.betdsi.eu/sportsbook-betting/nba-odds/',

            },

        ]

    }

    def __init__(self, *args, **kwargs):
        super(BetdsiSpider, self).__init__(*args, **kwargs)

        settings = get_project_settings()
        self.username = settings['BETDSI_USERNAME']
        self.password = settings['BETDSI_PASSWORD']
        self.cookies_file = settings['BETDSI_COOKIES_FILE']
        self.last_file = settings['BETDSI_LAST_HTML_FILE']
        self.user_agent = settings['USER_AGENT']

        self.api = Betdsi(
            self.username,
            self.password,
            user_agent=self.user_agent,
            cookies_file=self.cookies_file,
            last_html_file=self.last_file
        )

    def parse_game_odds(self, response):

        game = response.meta['game']


        #self.api.login()
        #yield {'loggedin' : self.api.is_logged_in()}
        scraped = False
        try:
            self.api.load_cookies()
            games = self.api.scrape_sport(sport=game)
            scraped = True
        except (RuntimeError, IOError):
            self.api.session.cookies = RequestsCookieJar()
            self.api.login()
            games = self.api.scrape_sport(sport=game)
            scraped = True
        finally:
            if scraped:
                for g in games:
                    yield GameOddsItem(g)
            self.log('Failed to scrape betdsi')

    def parse_mma_odds(self, response):
        for item_or_request in self.parse_game_odds(response):
            yield item_or_request

    def parse_ncaa_odds(self, response):

        for item_or_request in self.parse_game_odds(response):
            yield item_or_request

    def parse_nba_odds(self, response):

        for item_or_request in self.parse_game_odds(response):
            yield item_or_request

