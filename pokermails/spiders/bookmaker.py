# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

import datetime
import re
import time
from _ssl import SSLError

from scrapy import Selector
from scrapy.http import Request
from scrapy.loader import ItemLoader

from hyper import HTTPConnection
from furl import furl

from pokermails.items import GameOddsItem
from pokermails.spiders.base import BaseSpider

class BookmakerSpider(BaseSpider):
    name = 'bookmaker'
    allowed_domains = ['bookmaker.eu']
    start_urls = ['https://www.bookmaker.eu/sportsbook']

    games = {
        'mma': [
            {
                'parsing_method':'parse_mma_odds',
                'second_half' : False,
                'url' : 'https://www.bookmaker.eu/live-lines/martial-arts/mma-ufc',
            },

        ],
        'ncaa': [
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : False,
                'url' : 'https://www.bookmaker.eu/live-lines/basketball/college-basketball',
            },
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : True,
                'url' : 'https://www.bookmaker.eu/live-lines/basketball/ncaa-basketball-2nd-halfs'
            },
        ],
        'nba': [
            {
                'parsing_method' : 'parse_nba_odds',
                'second_half' : False,
                'url' : 'https://www.bookmaker.eu/live-lines/basketball/nba',
            },
            {
                'parsing_method' : 'parse_nba_odds',
                'second_half' : True,
                'url' : 'https://www.bookmaker.eu/live-lines/basketball/nba-2nd-halfs'
            },

        ]

    }

    def start_game_requests(self, response):

        def build_request(game):

            urls = []
            for i, g in enumerate(self.games[game]):
                parsing_method = self.get_parsing_method(game, i)
                if g['url']:

                    f = furl(g['url'])

                    ssl_error = None
                    max_retries = 5
                    current_tries = 0
                    while ssl_error == None or ssl_error != False or current_tries < max_retries:

                        h2 = HTTPConnection(str(f.netloc), port=443)
                        try:
                            h2.request('GET', str(f.path), headers={'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36'})

                            time.sleep(4)
                            h2_resp = h2.get_response()

                            if h2_resp.status != 200:
                                raise RuntimeError('bookmaker.eu returned non 200 status code')

                        except SSLError:
                            current_tries += 1
                            ssl_error = True
                            self.log('SSLError retrying again')
                            time.sleep(5)
                        else:
                            ssl_error = False

                            html = h2_resp.read()
                            with open('nba.html', 'w+') as f:
                                f.write(html)
                            urls.append((html, game, g['second_half'], parsing_method))
                            break

                    if ssl_error:
                        raise RuntimeError('Experienced {} SSLErrors while connectint to bookmaker.eu'.format(max_retries))

            return urls

        reqs = []
        if self.game is None:
            for game in self.games:
                reqs.extend(build_request(game))

        else:
            reqs.extend(build_request(self.game))

        for req in reqs:

            if req:
                html = req[0]
                game = req[1]
                second_half = req[2]
                parsing_method = req[3]

                for req_or_item in parsing_method(html, game=game, second_half=second_half):
                    yield req_or_item

    def parse_game_odds(self, html, **kwargs):

        game = kwargs['game']
        second_half = kwargs['second_half']

        scraped_at = datetime.datetime.fromtimestamp(
                time.time()).strftime('%Y-%m-%d %H:%M:%S')

        sel = Selector(text=html)

        trs = sel.xpath('//table[@id]//tr').extract()

        for game_raw in sel.xpath('//div[@class="matchup"]').extract():

            game_raw_sel = Selector(text=game_raw)
            self.log('found matchup')

            for odds_game_raw in game_raw_sel.xpath('//li[@class="odds"]').extract():
                self.log('found odds')

                odds_sel = Selector(text=odds_game_raw)

                game_item = GameOddsItem()
                game_item['game_date'] = sel.xpath('//div[@class="linesSubhead"]/span/text()').extract_first()
                game_item['game_time'] = game_raw_sel.xpath('//li[@class="time"]/span/text()').extract_first()
                game_item['scraped_at'] = scraped_at
                game_item['game_type'] = game
                game_item['site_url'] = self.allowed_domains[0]
                game_item['site_name'] = self.name
                game_item['second_half'] = 1 if second_half else 0
                game_item['team_one'] = {
                    'name' : odds_sel.xpath('//div[@class="oddsD hTeam"]//div[@class="team"]/h3/span/text()').extract_first(),
                    'juice' : odds_sel.xpath('//div[@class="oddsD hTeam"]//div[@class="spread"]/span/span/text()').extract_first(),
                    'spread' : odds_sel.xpath('//div[@class="oddsD hTeam"]//div[@class="spread"]/span/span/text()').extract_first(),
                    'money_line' : odds_sel.xpath('//div[@class="oddsD hTeam"]//div[@class="money"]/span/span/text()').extract_first(),
                }
                game_item['team_two'] = {
                    'name' : odds_sel.xpath('//div[@class="oddsD vTeam"]//div[@class="team"]/h3/span/text()').extract_first(),
                    'juice' : odds_sel.xpath('//div[@class="oddsD vTeam"]//div[@class="spread"]/span/span/text()').extract_first(),
                    'spread' : odds_sel.xpath('//div[@class="oddsD vTeam"]//div[@class="spread"]/span/span/text()').extract_first(),
                    'money_line' : odds_sel.xpath('//div[@class="oddsD vTeam"]//div[@class="money"]/span/span/text()').extract_first(),
                }

                yield game_item

    def parse_mma_odds(self, html, **kwargs):
        for req_or_item in self.parse_game_odds(html,**kwargs):
            yield req_or_item

    def parse_ncaa_odds(self, html, **kwargs):
        for req_or_item in  self.parse_game_odds(html, **kwargs):
            yield req_or_item

    def parse_nba_odds(self, html, **kwargs):
        for req_or_item in  self.parse_game_odds(html, **kwargs):
            yield req_or_item



