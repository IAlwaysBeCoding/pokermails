# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

import datetime
import json
import re
import time
import urllib

from scrapy import Selector
from scrapy.http import Request
from scrapy.loader import ItemLoader
from scrapy.loader.processors import SelectJmes

from pokermails.items import GameOddsItem
from pokermails.spiders.base import BaseSpider


class TopbetSpider(BaseSpider):
    name = 'topbet'
    allowed_domains = ['topbet.eu']
    start_urls = ['http://topbet.eu/']

    games = {
        'mma': [
            {
                'parsing_method':'parse_mma_odds',
                'second_half' : False,
                'url' : 'http://topbet.eu/sportsbook/'
            },
        ],
        'ncaa': [
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : False,
                'url' : 'http://topbet.eu/tbadmin43/sportsbook/show/basketball/ncaa-mens/2'
            },
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : True,
                'url' : 'http://topbet.eu/tbadmin43/sportsbook/show/basketball/ncaa-mens-2nd-half/2'
            }
        ],
        'nba': [
            {
                'parsing_method' : 'parse_nba_odds',
                'second_half' : False,
                'url' : 'http://topbet.eu/tbadmin43/sportsbook/show/basketball/nba/2'
            },
            {
                'parsing_method' : 'parse_nba_odds',
                'second_half' : True,
                'url' : 'http://topbet.eu/tbadmin43/sportsbook/show/basketball/nba-2nd-half/2'
            }

        ]

    }


    def parse_game_odds(self, response):

        game = response.meta['game']
        second_half = response.meta['second_half']

        raw_json = response.body
        jmes_query = '[].content[]' \


        for content in SelectJmes(jmes_query)(json.loads(raw_json)):

            scraped_at = datetime.datetime.fromtimestamp(
                time.time()).strftime('%Y-%m-%d %H:%M:%S')

            sel = Selector(text=content)
            for game_raw in sel.xpath('//div[@class="event"]').extract():
                game_raw_sel = Selector(text=game_raw)
                game_item = GameOddsItem()
                game_item['game_date'] = ''
                game_item['game_time'] = game_raw_sel.xpath('//h3/span/text()').extract_first()
                game_item['scraped_at'] = scraped_at
                game_item['game_type'] = game
                game_item['site_url'] = self.allowed_domains[0]
                game_item['site_name'] = self.name
                game_item['second_half'] = 1 if second_half else 0
                if game == 'mma':
                    game_item['team_one'] = {
                        'name' : game_raw_sel.xpath('//tr[@class="row0"]//td[contains(@class,"name")][1]/text()').extract_first(),
                        'juice' : '',
                        'spread' : '',
                        'money_line' : game_raw_sel.xpath('//tr[@class="row0"]//td[@class="spread_price"][1]/text()').extract_first()
                    }
                    game_item['team_two'] = {
                        'name' : game_raw_sel.xpath('//tr[@class="row0"]//td[contains(@class,"name")][2]/text()').extract_first(),
                        'juice' : '',
                        'spread' : '',
                        'money_line' : game_raw_sel.xpath('//tr[@class="row0"]//td[@class="spread_price"][2]/text()').extract_first()
                    }

                else:
                    game_item['team_one'] = {
                        'name' : game_raw_sel.xpath('//tr[@class="row0"]//td[contains(@class,"name")]/text()').extract_first(),
                        'juice' : game_raw_sel.xpath('//tr[@class="row0"]//td[@class="spread_price"]/text()').extract_first(),
                        'spread' : game_raw_sel.xpath('//tr[@class="row0"]//td[@class="spread"]/text()').extract_first(),
                        'money_line' : game_raw_sel.xpath('//tr[@class="row0"]//td[@class="money"]/text()').extract_first()
                    }
                    game_item['team_two'] = {
                        'name' : game_raw_sel.xpath('//tr[@class="row1"]//td[contains(@class,"name")]/text()').extract_first(),
                        'juice' : game_raw_sel.xpath('//tr[@class="row1"]//td[@class="spread_price"]/text()').extract_first(),
                        'spread' : game_raw_sel.xpath('//tr[@class="row1"]//td[@class="spread"]/text()').extract_first(),
                        'money_line' : game_raw_sel.xpath('//tr[@class="row1"]//td[@class="money"]/text()').extract_first()
                    }

                yield game_item

    def parse_mma_odds(self, response):
        for req_or_item in self.get_all_mma_games(response):
            yield req_or_item

    def parse_ncaa_odds(self, response):
        for req_or_item in  self.parse_game_odds(response):
            yield req_or_item

    def parse_nba_odds(self, response):
        for req_or_item in  self.parse_game_odds(response):
            yield req_or_item

    def get_all_mma_games(self, response):

        for mma_game in response.xpath('//li[@class="active has-sub"][.//span[text()="Mixed Martial Arts"]]//a/@href').extract():
            url = 'http://topbet.eu/tbadmin43/sportsbook/show/mma/{}/2'.format(mma_game.split('/')[-1])

            yield Request(response.urljoin(url),
                          meta=response.meta,
                          callback=self.parse_game_odds)

