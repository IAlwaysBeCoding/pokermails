# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

import datetime
import json
import time

from scrapy.http import Request
from scrapy.loader import ItemLoader
from scrapy.loader.processors import SelectJmes

from pokermails.items import GameOddsItem
from pokermails.spiders.base import BaseSpider


class BovadaSpider(BaseSpider):

    name = 'bovada'
    allowed_domains = ['bovada.lv']
    start_urls = ['https://bovada.lv']

    games = {
        'mma': [
            {
                'parsing_method':'parse_mma_odds',
                'second_half' : False,
                'url' : 'https://sports.bovada.lv/services/sports/event/v1/events/A/page' \
                        '/{start_page}..{end_page}/description/ufc-mma/ufc?showAll=true',
            },
            {
                'parsing_method':'parse_mma_odds',
                'second_half' : False,
                'url' : 'https://sports.bovada.lv/services/sports/event/v1/events/A/page' \
                        '/{start_page}..{end_page}/description/ufc-mma/ufc/ufc-futures?showAll=true',

            }

        ],
        'ncaa': [
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : False,
                'url' : 'https://sports.bovada.lv/services/sports/event/v1/events/A/page' \
                    '/{start_page}..{end_page}/description/basketball/college-basketball?showAll=true',

            }
        ],
        'nba': [
            {
                'parsing_method' : 'parse_nba_odds',
                'second_half' : False,
                'url' : 'https://sports.bovada.lv/services/sports/event/v1/events/A/page' \
                    '/{start_page}..{end_page}/description/basketball/nba?showAll=true',

            }
        ]

    }

    def __init__(self, *args, **kwargs):

        super(BovadaSpider, self).__init__(*args, **kwargs)
        self.start_page = kwargs.get('start_page', 0)
        self.pagination_size = kwargs.get('pagination_size', 20)

        if not isinstance(self.start_page,int) or not isinstance(self.pagination_size,int):
            raise TypeError('start_page or pagination_size must be an int')

    def start_game_requests(self, response):

        def build_request(game):

            urls = []
            for i, g in enumerate(self.games[game]):
                parsing_method = self.get_parsing_method(game, i)
                if g['url']:
                    game_odds_url = g['url'].format(start_page=self.start_page, end_page=self.pagination_size)


                    urls.append(Request(
                        game_odds_url,
                        meta={
                            'game' : game,
                            'start_page': self.start_page,
                            'url_template' : g['url'],
                            'end_page' : self.start_page + self.pagination_size
                        },
                        callback=parsing_method
                        )
                    )
            return urls


        reqs = []
        if self.game is None:
            for game in self.games:
                reqs.extend(build_request(game))

        else:
            reqs.extend(build_request(self.game))

        for req in reqs:
            yield req

    def parse_live_game(self, response):

        game_item = response.meta['game_item']
        teams = response.meta['teams']

        event_data = json.loads(response.body)
        min_periods = int(event_data['clock']['numberOfPeriods']) / 2
        if int(event_data['clock']['periodNumber']) > min_periods:
            second_half = 1
        else:

            second_half = 0

        teams.pop('is_live')
        game_item['second_half'] = second_half
        game_item.update(teams)
        yield game_item

    def parse_game_odds(self, response):

        start_page = response.meta['start_page']
        end_page = response.meta['end_page']
        url_template = response.meta['url_template']
        game_type = response.meta['game']

        raw_json = response.body
        data = json.loads(raw_json)

        game_times = SelectJmes("items[].startTime")(data)
        game_ids = SelectJmes("items[].id")(data)
        teams = SelectJmes(
            "items[] | [].{" \
                "is_live : displayGroups[].itemList[].periodType | [0]," \
                "team_one : { " \
                    "name       : competitors[?type==`HOME`].description | [0], " \
                    "spread 	: displayGroups[].itemList[?mainMarketType==`SPREAD`][].outcomes[?type==`H`][].price.handicap | [0], " \
                    "money_line : displayGroups[].itemList[?mainMarketType==`MONEYLINE`][].outcomes[?type==`H`][].price.american | [0], " \
                    "juice      : displayGroups[].itemList[?mainMarketType==`SPREAD`][].outcomes[?type==`H`][].price.american | [0]" \
                "}," \
                "team_two : { " \
                    "name       : competitors[?type==`AWAY`].description | [0], " \
                    "spread     : displayGroups[].itemList[?mainMarketType==`SPREAD`][].outcomes[?type==`A`][].price.handicap | [0], " \
                    "money_line : displayGroups[].itemList[?mainMarketType==`MONEYLINE`][].outcomes[?type==`A`][].price.american | [0], " \
                    "juice      : displayGroups[].itemList[?mainMarketType==`SPREAD`][].outcomes[?type==`A`][].price.american | [0]" \
                "}" \
            "}")(data)

        scraped_at = datetime.datetime.fromtimestamp(
            time.time()).strftime('%Y-%m-%d %H:%M:%S')

        for i, game_time in enumerate(game_times):
            game_item = GameOddsItem()
            game_item['scraped_at'] = scraped_at
            game_item['game_date'] = ''
            game_item['game_time'] = game_time
            game_item['game_type'] = game_type
            game_item['site_url'] = self.allowed_domains[0]
            game_item['site_name'] = self.name


            if teams[i]['is_live'] and teams[i]['is_live'] == 'Live Match':
                get_live_url = 'https://sports.bovada.lv/services/sports/results/api/v1/scores/{}'.format(game_ids[i])
                response.meta['game_item'] = game_item
                response.meta['teams'] = teams[i]
                yield Request(get_live_url,
                              meta=response.meta,
                              callback=self.parse_live_game)

            else:
                if teams[i]['is_live'] and teams[i]['is_live'] == 'Second Half':
                    game_item['second_half'] = 1
                else:
                    game_item['second_half'] = 0

                teams[i].pop('is_live')
                game_item.update(teams[i])

                yield game_item


        if game_times:
            start_page = start_page + self.pagination_size
            end_page = end_page + self.pagination_size
            game_odds_url = url_template.format(
                start_page=start_page, end_page=end_page)

            yield Request(game_odds_url,
                          meta={
                              'game': game_type,
                              'url_template': url_template,
                              'start_page': start_page,
                              'end_page': end_page,

                          },
                          callback=self.parse_game_odds)

    def parse_mma_odds(self, response):
        for item_or_request in self.parse_game_odds(response):
            yield item_or_request

    def parse_ncaa_odds(self, response):
        with open('bovada_ncaa.json', 'w+') as f:
            f.write(response.body)
        for item_or_request in self.parse_game_odds(response):
            yield item_or_request

    def parse_nba_odds(self, response):
        for item_or_request in self.parse_game_odds(response):
            yield item_or_request

