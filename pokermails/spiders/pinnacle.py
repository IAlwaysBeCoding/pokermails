# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

import datetime
import json
import re
import time
import urllib

from scrapy import Selector
from scrapy.http import Request
from scrapy.loader import ItemLoader
from scrapy.loader.processors import SelectJmes

from pokermails.items import GameOddsItem
from pokermails.spiders.base import BaseSpider


class PinnacleSpider(BaseSpider):
    name = 'pinnacle'
    allowed_domains = ['pinnacle.com']
    start_urls = ['https://pinnacle.com/']

    games = {
        'mma': [
            {
                'parsing_method':'parse_mma_odds',
                'second_half' : False,
                'url' : 'https://www.pinnacle.com/en/odds/match/mixed-martial-arts/bellator/bellator'
            },
            {
                'parsing_method':'parse_mma_odds',
                'second_half' : False,
                'url' : 'https://www.pinnacle.com/en/odds/match/mixed-martial-arts/ufc/ufc'
            }
        ],
        'ncaa': [
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : False,
                'url' : 'https://www.pinnacle.com/en/odds/match/basketball/usa/ncaa?sport=True'
            }
        ],
    'nba': [
            {
                'parsing_method' : 'parse_nba_odds',
                'second_half' : False,
                'url' : 'https://www.pinnacle.com/en/odds/match/basketball/usa/nba?sport=True'
            }
        ],

    }


    def parse_game_odds(self, response):

        game = response.meta['game']

        scraped_at = datetime.datetime.fromtimestamp(
            time.time()).strftime('%Y-%m-%d %H:%M:%S')

        raw_json = self.extract_json(response)
        jmes_query = 'Leagues[].Events[].{ ' \
           '"game_time" :  DateAndTime, ' \
           '"period" : PeriodNumber, ' \
           'team_one : {' \
           '    "name" : Participants[0].Name, ' \
           '    "juice" : Participants[0].Handicap.Price, ' \
           '    "spread" : Participants[0].Handicap.Min, ' \
           '    "money_line" : Participants[0].MoneyLine' \
           '}, ' \
           'team_two : {' \
           '    "name" : Participants[1].Name, ' \
           '    "juice": Participants[1].Handicap.Price, ' \
           '    "spread" : Participants[1].Handicap.Min, ' \
           '    "money_line" : Participants[1].MoneyLine' \
           '} ' \
       '}'


        for game_raw in SelectJmes(jmes_query)(json.loads(raw_json)):

            if game_raw['period'] == 1:
                continue

            game_item = GameOddsItem()
            game_item['game_date'] = ''
            game_item['game_time'] = game_raw['game_time']
            game_item['scraped_at'] = scraped_at
            game_item['game_type'] = game
            game_item['site_url'] = self.allowed_domains[0]
            game_item['site_name'] = self.name
            game_item['second_half'] = 1 if game_raw['period'] == 2 else 0
            game_item['team_one'] = {
                'name' : game_raw['team_one']['name'],
                'juice' : game_raw['team_one']['juice'],
                'spread' : game_raw['team_one']['spread'],
                'money_line' : game_raw['team_one']['money_line']
            }
            game_item['team_two'] = {
                'name' : game_raw['team_two']['name'],
                'juice' : game_raw['team_two']['juice'],
                'spread' : game_raw['team_two']['spread'],
                'money_line' : game_raw['team_two']['money_line']
            }

            yield game_item

    def parse_mma_odds(self, response):
        yield self.go_to_web_api(response)

    def parse_ncaa_odds(self, response):
        yield self.go_to_web_api(response)

    def parse_nba_odds(self, response):
        yield self.go_to_web_api(response)

    def go_to_web_api(self, response):

        games_top_id, games_league_id = self.get_games_id(response)

        if not (games_top_id and games_league_id):
            raise RunTimeError('Could not parse games top id and/or games league id')

        api_url = self.build_api_url(games_top_id, games_league_id)

        return Request(api_url,
                      meta=response.meta,
                      callback=self.parse_game_odds)

    def get_games_id(self, response):

        games_top_id = None
        games_league_id = None
        guest_lines = response.xpath('//div[@ng-controller="GuestLinesController"]/@ng-init').extract_first()

        if guest_lines:
            get_games_id = re.search(r'(?<=init\()(.*?), (.*?)(?=, )', guest_lines, re.DOTALL)

            if get_games_id:
                games_top_id = get_games_id.group(1)
                games_league_id = get_games_id.group(2)

        return games_top_id, games_league_id

    def build_api_url(self, games_top_id, games_league_id):
        return 'https://www.pinnacle.com/webapi/1.17/api/v1/GuestLines' \
            '/Deadball/{}/{}?callback=angular.callbacks._0'.format(games_top_id, games_league_id)

    def extract_json(self, response):
        got_raw_json = re.search(r'(?<=\({)(.*?)(?=}\);)', response.body, re.DOTALL)

        if got_raw_json:
            return '{' + got_raw_json.group(0) + '}'

