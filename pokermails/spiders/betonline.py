# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import json

import datetime
import time
import urllib

from scrapy import Selector
from scrapy.http import Request
from scrapy.loader import ItemLoader

from pokermails.items import GameOddsItem
from pokermails.spiders.base import BaseSpider


class BetonlineSpider(BaseSpider):
    name = 'betonline'
    allowed_domains = ['betonline.ag']
    start_urls = ['https://www.betonline.ag']

    games = {
        'mma': [
            {
                'parsing_method':'parse_mma_odds',
                'second_half' : False,
                'url' : 'https://www.betonline.ag/sportsbook/martial-arts/mma',
                'sport_category' : 'Martial Arts',
                'sport_name' : 'MMA'
            },
        ],
        'ncaa': [
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : False,
                'url' : 'https://www.betonline.ag/sportsbook/basketball/ncaa',
                'sport_category' : 'Basketball',
                'sport_name' : 'NCAA'

            },
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : True,
                'url' : 'https://www.betonline.ag/sportsbook/basketball/ncaa',
                'sport_category' : 'Basketball',
                'sport_name' : 'NCAA'

            },
        ],
        'nba': [
            {
                'parsing_method' : 'parse_nba_odds',
                'second_half' : False,
                'url' : 'https://www.betonline.ag/sportsbook/basketball/nba',
                'sport_category' : 'Basketball',
                'sport_name' : 'NBA'

            },
            {
                'parsing_method' : 'parse_nba_odds',
                'second_half' : True,
                'url' : 'https://www.betonline.ag/sportsbook/basketball/nba',
                'sport_category' : 'Basketball',
                'sport_name' : 'NBA'

            },

        ]
    }


    def start_game_requests(self, response):

        yield Request('https://www.betonline.ag/sportsbook',
                        meta={
                            'game' : self.game,
                        },
                        callback=self.parse_main_page)

    def parse_game_odds(self, response):

        game = response.meta['game']
        second_half = response.meta['second_half']
        scraped_at = datetime.datetime.fromtimestamp(
            time.time()).strftime('%Y-%m-%d %H:%M:%S')

        game_dates = {}
        game_dates_index = []

        for i, game_date in enumerate(response.xpath('//table[@id="contestDetailTable"]//tbody').extract()):
            sel = Selector(text=game_date)

            get_date = sel.xpath('//tbody[@class="date expanded" or @class="date"]//td/text()').extract_first()
            if get_date:
                game_dates[i] = get_date
                game_dates_index.append(i)


        last_game_date = None

        for i, game_raw in enumerate(response.xpath('//table[@id="contestDetailTable"]//tbody').extract()):

            sel = Selector(text=game_date)
            if not last_game_date:
                last_game_date = game_dates[i]
                continue

            if last_game_date and i in game_dates:
                last_game_date = game_dates[i]
                continue

            has_juice_spread = True

            if game == 'mma':
                has_juice_spread = False

            sel = Selector(text=game_raw)
            game_item = GameOddsItem()
            game_item['game_date'] = last_game_date
            game_item['game_time'] = sel.xpath('//td[@class="col_time bdevtt"]/text()').extract_first()
            game_item['scraped_at'] = scraped_at
            game_item['game_type'] =  game
            game_item['site_url'] =  self.allowed_domains[0]
            game_item['site_name'] =  self.name
            game_item['second_half'] =  '1' if second_half else '0'
            game_item['team_one'] = {
                'name' : sel.xpath('//tr[@class="h2hSeq firstline"]//td[@class="col_teamname bdevtt"]/text()').extract_first(),
                'juice' : sel.xpath('//tr[@class="h2hSeq firstline"]//td[@class="odds bdevtt displayOdds"]/text()').extract_first() if has_juice_spread else '',
                'spread' : sel.xpath('//tr[@class="h2hSeq firstline"]//td[contains(@class, "hdcp bdevtt")]/text()').extract_first() if has_juice_spread else '',
                'money_line' : sel.xpath('//tr[@class="h2hSeq firstline"]//td[@class="odds bdevtt moneylineodds displayOdds"]/text()').extract_first()
            }
            game_item['team_two'] = {
                'name' : sel.xpath('//tr[@class="otherline"]//td[@class="col_teamname bdevtt"]/text()').extract_first(),
                'juice' : sel.xpath('//tr[@class="otherline"]//td[@class="odds bdevtt displayOdds"]/text()').extract_first() if has_juice_spread else '',
                'spread' : sel.xpath('//tr[@class="otherline"]//td[contains(@class,"hdcp bdevtt")]/text()').extract_first() if has_juice_spread else '',
                'money_line' : sel.xpath('//tr[@class="otherline"]//td[@class="odds bdevtt moneylineodds displayOdds"]/text()').extract_first()
            }

            yield game_item

    def parse_mma_odds(self, response):
        for item_or_request in self.parse_game_odds(response):
            yield item_or_request

    def parse_ncaa_odds(self, response):

        for item_or_request in self.parse_game_odds(response):
            yield item_or_request

    def parse_nba_odds(self, response):

        for item_or_request in self.parse_game_odds(response):
            yield item_or_request

    def parse_main_page(self, response):

        def build_request(game):

            for i, g in enumerate(self.games[game]):
                parsing_method = self.get_parsing_method(game, i)

                body = {
                    'param.SortOption' : 'L',
                    'param.PrdNo' : '2' if g['second_half'] else '0',
                    'param.Type' :'H2H'
                }

                body['param.H2HParam.Lv1'] = g['sport_category']
                body['param.H2HParam.Lv2'] = g['sport_name']
                body = urllib.urlencode(body)

                headers={
                    'X-Requested-With' : 'XMLHttpRequest',
                    'Accept' : '*/*',
                    'Accept-Encoding' : 'gzip, deflate, br',
                    'Accept-Language' : 'en-US,en;q=0.9',
                    'Connection' : 'keep-alive',
                    'Content-Type' : 'application/x-www-form-urlencoded',
                    'Origin' : 'https://www.betonline.ag',
                    'Referer' : 'https://www.betonline.ag/sportsbook',
                }
                return Request(
                    'https://www.betonline.ag/sportsbook/Line/RetrieveLineData',
                    method='POST',
                    meta={
                        'game' : game,
                        'second_half' : g['second_half']
                    },
                    body=body,
                    headers=headers,
                    callback=parsing_method)

        reqs = []
        if self.game is None:
            for game in self.games:
                reqs.append(build_request(game))

        else:
            reqs.append(build_request(self.game))


        for req in reqs:
            yield req


