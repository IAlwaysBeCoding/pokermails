import sys
reload(sys)
sys.setdefaultencoding('utf8')

import datetime
import json
import time
import urllib

from scrapy import Selector
from scrapy.http import Request
from scrapy.loader import ItemLoader

from pokermails.items import GameOddsItem
from pokermails.spiders.base import BaseSpider


class ThegreekSpider(BaseSpider):

    name = 'thegreek'
    allowed_domains = ['thegreek.com']
    start_urls = ['http://thegreek.com/']

    games = {
        'mma': [
            {
                'parsing_method' : 'parse_mma_odds',
                'second_half' : False,
                'url' : 'http://www.thegreek.com/sportsbook/sport/martial%20arts',
            }
        ],
        'ncaa': [
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : False,
                'url' : 'http://www.thegreek.com/sportsbook/betting-lines/Basketball/NCAA'
            },

        ],
        'nba': [
            {
                'parsing_method' : 'parse_nba_odds',
                'second_half' : False,
                'url' : 'http://www.thegreek.com/sportsbook/betting-lines/Basketball/NBA'
            },

        ]

    }



    def parse_game_odds(self, response):

        game = response.meta['game']
        scraped_at = datetime.datetime.fromtimestamp(
            time.time()).strftime('%Y-%m-%d %H:%M:%S')
        for game_raw in response.xpath('//div[@class="lines"]').extract():
            sel = Selector(text=game_raw)
            game_item = GameOddsItem()
            game_item['game_date'] = response.xpath('//span[@class="icon date"]/text()').extract_first()
            game_item['game_time'] = sel.xpath('//span[@class="icon time"]/text()').extract_first()
            game_item['scraped_at'] = scraped_at
            game_item['game_type'] =  game
            game_item['site_url'] =  self.allowed_domains[0]
            game_item['site_name'] =  self.name

            has_second_half = sel.xpath('//ul[contains(@class, "row-away")]//li[@class="period"]/' \
                                        'span[@class="" and text()="2nd"]/following-sibling::sub[text()="half"]').extract_first()
            game_item['second_half'] = 1 if has_second_half else 0
            game_item['team_one'] = {
                'name' : sel.xpath('//ul[contains(@class,"row-home")]//li[@class="name"]//a/text()').extract_first(),
                'juice' : sel.xpath('//li[@id="spread_home" and starts-with(@class, "spread")]//a[2]/text()').extract_first(),
                'spread' : sel.xpath('//li[@id="spread_home" and starts-with(@class, "spread")]//a[1]/text()').extract_first(),
                'money_line' : sel.xpath('//li[@id="spread_home" and starts-with(@class, "money_line")]//a[1]/text()').extract_first(),
            }
            game_item['team_two'] = {
                'name' : sel.xpath('//ul[contains(@class,"row-away")]//li[@class="name"]//a/text()').extract_first(),
                'juice' : sel.xpath('//li[@id="spread_away" and starts-with(@class, "spread")]//a[2]/text()').extract_first(),
                'spread' : sel.xpath('//li[@id="spread_away" and starts-with(@class, "spread")]//a[1]/text()').extract_first(),
                'money_line' : sel.xpath('//li[@id="spread_away" and starts-with(@class, "money_line")]//a[1]/text()').extract_first(),
            }

            yield game_item

    def parse_listing_page(self, response):

        parsing_method = response.meta['parsing_method']
        sel = Selector(response)

        listing_urls = []

        for url in sel.xpath('//ul[@class="landing-list landing-page"]//li[@class="nested first"]/a/@href').extract():
            yield Request(response.urljoin(url),
                          meta=response.meta,
                          callback=parsing_method)

    def parse_games_odds_moneyline(self, response):
        game = response.meta['game']

        scraped_at = datetime.datetime.fromtimestamp(
            time.time()).strftime('%Y-%m-%d %H:%M:%S')

        for game_raw in response.xpath('//div[@class="lines lines-props"]').extract():
            sel = Selector(text=game_raw)
            game_item = GameOddsItem()
            game_item['game_date'] = response.xpath('//span[@class="icon time  left small-gray"]/text()').extract_first()
            game_item['game_time'] = ''
            game_item['scraped_at'] = scraped_at
            game_item['game_type'] =  game
            game_item['site_url'] =  self.allowed_domains[0]
            game_item['site_name'] =  self.name
            game_item['second_half'] = 0
            game_item['team_one'] = {
                'name': sel.xpath('//ul[not(contains(@class,"row-away"))]//span[@class="name"]/text()').extract_first(),
                'juice' : '',
                'spread' : '',
                'money_line' : sel.xpath('//ul[not(contains(@class,"row-away"))]//span[@class="odd"]/text()').extract_first(),
            }
            game_item['team_two'] = {
                'name': sel.xpath('//ul[contains(@class,"row-away")]//span[@class="name"]/text()').extract_first(),
                'juice' : '',
                'spread' : '',
                'money_line' : sel.xpath('//ul[contains(@class,"row-away")]//span[@class="odd"]/text()').extract_first(),
            }

            yield game_item

    def parse_mma_odds(self, response):
        response.meta['parsing_method'] = self.parse_games_odds_moneyline

        for item_or_request in self.parse_listing_page(response):
            yield item_or_request

    def parse_ncaa_odds(self, response):

        for item_or_request in self.parse_game_odds(response):
            yield item_or_request

    def parse_nba_odds(self, response):

        for item_or_request in self.parse_game_odds(response):
            yield item_or_request

