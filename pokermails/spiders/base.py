# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf8')

from scrapy import Spider
from scrapy.http import Request

class MissingParsingMethod(Exception):
    pass


class BaseSpider(Spider):

    name = 'base_spider'
    start_urls = []
    games = {
        'mma': [
            {
                'parsing_method':'parse_mma_odds',
                'second_half' : False,
                'url' : None
            }
        ],
        'ncaa': [
            {
                'parsing_method' : 'parse_ncaa_odds',
                'second_half' : False,
                'url' : None
            }
        ]
    }


    def __init__(self, *args, **kwargs):
        game = kwargs.get('game', None)
        self._verify_valid_game(game)
        self.game = game

        super(BaseSpider, self).__init__(*args, **kwargs)

    def _verify_valid_game(self, game):

        if game not in self.games and game != None :
            raise TypeError(
                'game needs to be one of the following values :{}'.format(
                    ' or '.join(
                        self.games.keys())))

    def _verify_parsing_method_exists(self, game, index):

        parsing_method = getattr(self, self.games[game][index]['parsing_method'], None)

        if not parsing_method:
            raise MissingParsingMethod(
                'Missing the implementation of self.{} method'.format(
                    self.games[game]['parsing_method']))

    def get_parsing_method(self, game, index):

        self._verify_parsing_method_exists(game, index)
        return getattr(self, self.games[game][index]['parsing_method'], None)

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url,
                          callback=self.start_game_requests,
                          dont_filter=True)

    def start_game_requests(self, response):

        def build_request(game):

            urls = []
            for i, g in enumerate(self.games[game]):
                parsing_method = self.get_parsing_method(game, i)
                if g['url']:
                    urls.append(Request(
                        g['url'],
                        meta={
                            'game' : game,
                            'second_half' : g['second_half']
                        },
                        callback=parsing_method
                        )
                    )

            return urls

        reqs = []
        if self.game is None:
            for game in self.games:
                reqs.extend(build_request(game))

        else:
            reqs.extend(build_request(self.game))

        for req in reqs:
            yield req

    def parse_mma_odds(self, response):
        raise NotImplementedError('Please implement "parse_mma_odds"')

    def parse_ncaa_odds(self, response):
        raise NotImplementedError('Please implement "parse_ncaa_odds"')
    def parse_nba_odds(self, response):
        raise NotImplementedError('Please implement "parse_nba_odds"')
