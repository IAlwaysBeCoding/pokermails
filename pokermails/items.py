# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field

class GameOddsItem(Item):

    scraped_at = Field()
    game_date = Field()
    game_time = Field()
    game_type = Field()
    site_url = Field()
    site_name = Field()
    second_half = Field()
    team_one = Field()
    team_two = Field()
